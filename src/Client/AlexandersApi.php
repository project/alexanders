<?php

namespace Drupal\alexanders\Client;

use Drupal\alexanders\Entity\AlexandersOrder;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use Drupal\Core\Url;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Defines functions for passing data to the Alexanders API.
 *
 * @package Drupal\alexanders\Client
 */
class AlexandersApi implements AlexandersApiInterface {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * @var \GuzzleHttp\Client|\GuzzleHttp\ClientInterface
   */
  private $client;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * AlexandersApi constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The alexanders logging channel.
   */
  public function __construct(ConfigFactoryInterface $config_factory = NULL, ClientInterface $http_client = NULL, LoggerChannelInterface $logger = NULL) {
    if (!$config_factory) {
      $config_factory = \Drupal::configFactory();
    }
    $this->config = $config_factory->get('alexanders.settings');

    if (!$http_client) {
      $http_client = \Drupal::httpClient();
    }
    $http_client_config = $http_client->getConfig();
    // Inject a Guzzle middleware to generate verbose output for every request
    // performed if configured.
    /** @var \GuzzleHttp\HandlerStack $handler */
    $handler = clone $http_client->getConfig('handler');
    $handler->push($this->getResponseLogHandler());
    $http_client_config['handler'] = $handler;
    $this->client = new Client($http_client_config);

    if (!$logger) {
      $logger = \Drupal::logger('alexanders');
    }
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function createOrder(AlexandersOrder $order, $throw_exception = FALSE) {
    $orderData = $this->buildOrderData($order);
    $url = $this->generateUrl()->toString();
    try {
      $response = $this->client->post($url, $orderData);
    }
    catch (RequestException $e) {
      watchdog_exception('alexanders', $e);
      if ($throw_exception) {
        throw $e;
      }
      return FALSE;
    }
    return $this->processResponse($response, $throw_exception);
  }

  /**
   * {@inheritdoc}
   */
  public function updateOrder(AlexandersOrder $order, $throw_exception = FALSE) {
    $orderData = $this->buildOrderData($order, TRUE);
    $url = $this->generateUrl($order)->toString();
    try {
      // Update shipping address.
      // Modify data, keep headers.
      $shipping_data = $orderData;
      $shipping_data['json'] = $orderData['json']['shipping']['address'];
      $response = $this->client->put($url, $shipping_data);
      // Update ShipMethod.
      if ($response->getStatusCode() == 200) {
        // Change the destination to update shipping method.
        $url_parts = explode('/', $url);
        $order_id = array_pop($url_parts);
        // Remove the version part of the URL. This is all very brittle. Ideally
        // we'd configure a separate v1.1 url or something.
        array_pop($url_parts);
        $url_parts[] = 'v1.1';
        $url_parts[] = 'shippingmethod';
        $url_parts[] = $order_id;
        $url = implode('/', $url_parts);
        // Modify data, keep headers.
        $shipping_data = $orderData;
        $shipping_data['json']['shipMethod'] = $orderData['json']['shipping']['shipMethod'];
        $response = $this->client->put($url, $shipping_data);
      }
    }
    catch (RequestException $e) {
      watchdog_exception('alexanders', $e);
      if ($throw_exception) {
        throw $e;
      }
      return FALSE;
    }
    return $this->processResponse($response, $throw_exception);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOrder($order, $throw_exception = FALSE) {
    $url = $this->generateUrl($order)->toString();
    try {
      $response = $this->client->delete($url);
    }
    catch (RequestException $e) {
      watchdog_exception('alexanders', $e);
      if ($throw_exception) {
        throw $e;
      }
      return FALSE;
    }
    return $this->processResponse($response, $throw_exception);
  }

  /**
   * Aggregates order data into a format the Alexander API expects.
   *
   * @param \Drupal\alexanders\Entity\AlexandersOrder $order
   *   Order ID to build request data off of.
   *
   * @return array|bool
   *   Return array if successful, FALSE if order can't be loaded.
   */
  protected function buildOrderData(AlexandersOrder $order) {
    $data = [
      'headers' => ['X-API-KEY' => $this->config->get('client_apikey')],
      'json' => [
        'orderKey1' => $order->id(),
        'orderKey2' => $order->label(),
        'rushOrder' => $order->getRush(),
        'standardPrintItems' => $order->exportPrintItems(),
        'photobookItems' => $order->exportPhotobooks(),
        'inventoryItems' => $order->exportInventoryItems(),
        'nonBillableReprint' => $order->getNonBillableReprint(),
        'notebookItems' => $order->exportNotebooks(),
      ],
    ];
    /** @var \Drupal\alexanders\Entity\AlexandersShipment $shipment */
    $shipment = $order->getShipment()[0];
    $data['json']['shipping'] = $shipment->export();
    if ($this->config->get('verbose_log')) {
      $logdata = $data;
      $logdata['json'] = json_encode($logdata['json'], JSON_PRETTY_PRINT);
      $this->logger->notice('Data sent to Alexanders <pre>@data</pre>', ['@data' => print_r($logdata, TRUE)]);
    }
    return $data;
  }

  /**
   * Process responses from the Alexanders API.
   *
   * @param \GuzzleHttp\Psr7\Response $response
   *   Response object to process.
   * @param bool $throw_exception
   *   If TRUE then an exception will be thrown instead of returning FALSE.
   *
   * @return bool
   *   TRUE if the request was a success, FALSE otherwise.
   */
  protected function processResponse(Response $response, $throw_exception = FALSE) {
    $code = $response->getStatusCode();
    if ($code == 200) {
      return TRUE;
    }

    if ($throw_exception) {
      throw new \RuntimeException(sprintf('Response code %s, assuming NOT OKAY', $code));
    }
    return FALSE;
  }

  /**
   * Determine if we're using a sandbox or otherwise and give correct URL.
   *
   * @param \Drupal\alexanders\Entity\AlexandersOrder $order
   *   Order entity to build URL for.
   *
   * @return \Drupal\Core\Url
   *   URL to send requests to.
   */
  protected function generateUrl(AlexandersOrder $order = NULL) {
    if ($this->config->get('enable_sandbox') || !$this->config->get('client_apikey')) {
      $urlString = $this->config->get('api_sandbox_url');
    }
    else {
      $urlString = $this->config->get('api_url');
    }
    if ($order) {
      $urlString .= '/' . $order->id();
    }
    return Url::fromUri($urlString);
  }

  /**
   * Provides a Guzzle middleware handler to log every response received.
   *
   * @return callable
   *   The callable handler that will do the logging.
   */
  protected function getResponseLogHandler() {
    return function (callable $handler) {
      return function (RequestInterface $request, array $options) use ($handler) {
        // Data sent is logged by buildOrderData so only log responses.
        return $handler($request, $options)
          ->then(function (ResponseInterface $response) use ($request) {
            if ($this->config->get('verbose_log')) {
              $this->logger->notice(
                '@code response from Alexanders (@method to @url: <pre>@headers</pre><br/><pre>@response</pre>)',
                [
                  '@code' => $response->getStatusCode(),
                  '@method' => $request->getMethod(),
                  '@url' => (string) $request->getUri(),
                  '@headers' => print_r($response->getHeaders(), TRUE),
                  '@response' => $response->getBody(),
                ]
              );
            }
            return $response;
          });
      };
    };
  }

}
