<?php

namespace Drupal\alexanders\Client;

use Drupal\alexanders\Entity\AlexandersOrder;

/**
 * Sends API requests to Alexanders.
 */
interface AlexandersApiInterface {

  /**
   * Sent a POST request to the API to create an order.
   *
   * @param \Drupal\alexanders\Entity\AlexandersOrder $order
   *   AlexandersOrder to send.
   * @param bool $throw_exception
   *   If TRUE then any exceptions caused by making the request will be
   *   rethrown.
   *
   * @return bool
   *   TRUE if we created the order, FALSE otherwise.
   */
  public function createOrder(AlexandersOrder $order, $throw_exception = FALSE);

  /**
   * Sent a POST request to the API to create an order.
   *
   * @param \Drupal\alexanders\Entity\AlexandersOrder $order
   *   AlexandersOrder to send.
   * @param bool $throw_exception
   *   If TRUE then any exceptions caused by making the request will be
   *   rethrown.
   *
   * @return bool
   *   TRUE if we created the order, FALSE otherwise.
   */
  public function updateOrder(AlexandersOrder $order, $throw_exception = FALSE);

  /**
   * Sent a DELETE request to the API to delete an order.
   *
   * @param \Drupal\alexanders\Entity\AlexandersOrder $order
   *   AlexandersOrder to delete.
   * @param bool $throw_exception
   *   If TRUE then any exceptions caused by making the request will be
   *   rethrown.
   *
   * @return bool
   *   TRUE if we deleted the order, FALSE otherwise.
   */
  public function deleteOrder($order, $throw_exception = FALSE);

}
