<?php

namespace Drupal\alexanders\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Alexanders Notebook (e.g guestbook) item.
 *
 * @ContentEntityType(
 *   id = "alexanders_notebook",
 *   label = @Translation("Alexanders Notebook"),
 *   label_singular = @Translation("Alexanders Notebook"),
 *   label_plural = @Translation("Alexanders Notebooks"),
 *   label_count = @PluralTranslation(
 *     singular = "@count notebook",
 *     plural = "@count notebooks",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *     },
 *   },
 *   base_table = "alexanders_notebook",
 *   data_table = "alexanders_notebook_data",
 *   admin_permission = "administer site settings",
 *   fieldable = TRUE,
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "item_id",
 *     "label" = "sku",
 *   },
 * )
 */
class AlexandersNotebookItem extends ContentEntityBase implements AlexandersNotebookItemInterface {

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getSku() {
    return $this->get('sku')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function setSku($sku) {
    $this->set('sku', $sku);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setQuantity(int $quantity) {
    $this->set('quantity', $quantity);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getQuantity() {
    return $this->get('sku')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function setCoverUrl(string $url) {
    $this->set('coverUrl', $url);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getCoverUrl() {
    return $this->get('coverUrl')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function setFoilUrl(string $url) {
    $this->set('foilUrl', $url);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getFoilUrl() {
    return $this->get('foilUrl')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function setFoilColor(string $color) {
    $this->set('foilColor', $color);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getFoilColor() {
    return $this->get('foilColor')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function setPageStyle(string $style) {
    $this->set('pageStyle', $style);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getPageStyle() {
    return $this->get('pageStyle')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function setFinish(string $finish) {
    $this->set('finish', $finish);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getFinish() {
    return $this->get('finish')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function setPageCount(int $count) {
    $this->set('pageCount', $count);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getPageCount() {
    return $this->get('pageCount')->value;
  }

  /**
   * {@inheritDoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Item description'))
      ->setRequired(FALSE)
      ->setSetting('display_description', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['sku'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Item SKU'))
      ->setRequired(TRUE)
      ->setSetting('display_description', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['quantity'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Quantity'))
      ->setDescription(t('Quantity of items.'))
      ->setRequired(TRUE)
      ->setSetting('display_description', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['coverUrl'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Cover URL'))
      ->setRequired(TRUE)
      ->setSetting('display_description', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['foilUrl'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Foil URL'))
      ->setRequired(FALSE)
      ->setSetting('display_description', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['foilColor'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Foil Color'))
      ->setRequired(FALSE)
      ->setSetting('display_description', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['pageStyle'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Page Style'))
      ->setRequired(TRUE)
      ->setSetting('display_description', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['finish'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Notebook Finish'))
      ->setRequired(FALSE)
      ->setSetting('display_description', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['pageCount'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Notebook Pagecount'))
      ->setRequired(FALSE)
      ->setSetting('display_description', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
