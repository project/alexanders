<?php

namespace Drupal\alexanders\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines function for AlexandersOrder entities.
 *
 * @package Drupal\alexanders\Entity
 */
interface AlexandersNotebookItemInterface extends ContentEntityInterface {

  /**
   * Get item's description.
   *
   * @return string
   */
  public function getDescription();

  /**
   * Set item's description.
   *
   * @param string $description
   *   Description for item.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Get SKU of product in order item.
   *
   * @return string
   *   Product stock keeping unit.
   */
  public function getSku();

  /**
   * Set SKU of order item.
   *
   * @param string $sku
   *   Stock keeping unit.
   *
   * @return $this
   */
  public function setSku($sku);

  /**
   * Set quantity of notebooks.
   *
   * @param int $quantity
   *
   * @return $this
   */
  public function setQuantity(int $quantity);

  /**
   * Get quantity of notebooks.
   *
   * @return int
   */
  public function getQuantity();

  /**
   * Set cover file URL.
   *
   * @param string $url
   *   Location of cover image.
   *
   * @return $this
   */
  public function setCoverUrl(string $url);

  /**
   * Get cover URL.
   *
   * @return string
   */
  public function getCoverUrl();

  /**
   * Set foil file URL.
   *
   * @param string $url
   *   Location of cover image.
   *
   * @return $this
   */
  public function setFoilUrl(string $url);

  /**
   * Get foil URL.
   *
   * @return string
   */
  public function getFoilUrl();

  /**
   * Set the foil colour, one of [GOLD, ROSEGOLD, SILVER].
   *
   * @param string $color
   *   The foil colour.
   *
   * @return $this
   */
  public function setFoilColor(string $color);

  /**
   * Get foil colour.
   *
   * @return string
   */
  public function getFoilColor();

  /**
   * Set the page style, one of [WHITE, KRAFT, BLACK, LINED].
   *
   * @param string $style
   *   The style to set.
   *
   * @return $this
   */
  public function setPageStyle(string $style);

  /**
   * Get the page style.
   *
   * @return string
   */
  public function getPageStyle();

  /**
   * Set the notebook finish, one of [MATTE, VELVET, GLOSS].
   *
   * @param string $finish
   *   The finish.
   *
   * @return $this
   */
  public function setFinish(string $finish);

  /**
   * Get the notebook finish.
   *
   * @return string
   */
  public function getFinish();

  /**
   * Set the page count.
   *
   * @param int $count
   *   The number of pages within this notebook.
   *
   * @return $this
   */
  public function setPageCount(int $count);

  /**
   * Get the notebook page count.
   *
   * @return int
   */
  public function getPageCount();

}
