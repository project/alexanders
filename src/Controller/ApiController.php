<?php

namespace Drupal\alexanders\Controller;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Drupal\alexanders\Entity\AlexandersOrder;
use Drupal\alexanders\Event\ApiStatusEvent;
use Drupal\alexanders\Event\ApiErrorEvent;
use Drupal\alexanders\Event\ApiShipmentEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Handles incoming API requests from Alexanders.
 *
 * @package Drupal\alexanders\Controller
 */
class ApiController extends ControllerBase {

  private $eventDispatcher;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * ApiController constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   */
  public function __construct(EventDispatcherInterface $event_dispatcher, RequestStack $request_stack, LoggerChannelInterface $logger) {
    $this->eventDispatcher = $event_dispatcher;
    $this->requestStack = $request_stack;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('request_stack'),
      $container->get('logger.channel.alexanders')
    );
  }

  /**
   * Updates 'due date' parameter of Alexanders order.
   *
   * @param \Drupal\alexanders\Entity\AlexandersOrder $order
   *   Order ID.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Incoming request object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Returns status code based on result.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function orderPrintingUpdate(AlexandersOrder $order, Request $request) {
    $json = $this->getJson($request, ['dueDate']);
    $due_date = strtotime($json->dueDate);
    $order->setDue($due_date);

    if ($this->inLiveMode()) {
      $order->save();
      $event = new ApiStatusEvent($order, $due_date);
      $this->eventDispatcher->dispatch(ApiStatusEvent::EVENT_NAME, $event);
    }
    return new Response();
  }

  /**
   * Updates order shipment status.
   *
   * @param \Drupal\alexanders\Entity\AlexandersOrder $order
   *   Order ID.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Incoming request object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Returns status code based on result.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function orderShippedUpdate(AlexandersOrder $order, Request $request) {
    /** @var \Drupal\alexanders\Entity\AlexandersShipment $shipment */
    $shipment = $order->getShipment()[0];

    $json = $this->getJson($request, ['trackingNumber', 'dateShipped', 'cost', 'shipMethod', 'carrier']);

    if ($this->inLiveMode()) {
      $shipdate = strtotime($json->dateShipped);
      $shipment->setTimestamp($shipdate);
      $shipment->setCost($json->cost);
      $shipment->setTracking($json->trackingNumber);
      $shipment->save();

      $event = new ApiShipmentEvent($order, $json->shipMethod, $json->carrier, $json->trackingNumber, $shipdate, $json->cost);
      $this->eventDispatcher->dispatch(ApiShipmentEvent::EVENT_NAME, $event);
    }

    return new Response();
  }

  /**
   * Logs an error associated with the item for manual review.
   *
   * @param \Drupal\alexanders\Entity\AlexandersOrder $order
   *   Order ID.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Incoming request object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Returns status code based on result.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function orderErrorUpdate(AlexandersOrder $order, Request $request) {
    $json = $this->getJson($request, ['message', 'itemKey']);

    if ($this->inLiveMode()) {
      // Placeholder api key value, replace with randomly generated value.
      $this->logger
        ->error('Alexanders API sent error processing order @orderid: @message',
          [
            '@orderid' => $order->id(),
            '@message' => $json->message,
          ]);

      $event = new ApiErrorEvent($order, $json->itemKey, $json->message);
      $this->eventDispatcher->dispatch(ApiErrorEvent::EVENT_NAME, $event);
    }
    // Return 200.
    return new Response();
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param string[] $required_properties
   *
   * @return mixed
   */
  protected function getJson(Request $request, $required_properties = []) {
    $json = json_decode($request->getContent());
    if (!$json) {
      throw new BadRequestHttpException('Invalid JSON');
    }
    $missing_properties = array_diff($required_properties, array_keys(get_object_vars($json)));
    if (!empty($missing_properties)) {
      throw new BadRequestHttpException(sprintf('Invalid JSON, missing %s', implode(', ', $missing_properties)));
    }
    return $json;
  }

  /**
   * Checks whether provided API key is correct.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  public function checkAccess() {
    $headers = $this->requestStack->getMasterRequest()->headers;
    if (!$headers->has('X-API-KEY')) {
      return AccessResult::forbidden('Missing X-API-KEY');
    }

    // Check the key.
    $config = $this->config('alexanders.settings');
    $api_key = $this->inLiveMode() ? $config->get('real_api_key') : $config->get('sandbox_api_key');
    $valid_key = strlen($api_key) > 0 && Crypt::hashEquals($headers->get('X-API-KEY'), $api_key);
    if (!$valid_key) {
      return AccessResult::forbidden('Invalid X-API-KEY')->addCacheableDependency($config);
    }

    return AccessResult::allowed()->addCacheableDependency($config);
  }

  /**
   * Determines if the site is in the live mode.
   *
   * @return bool
   */
  protected function inLiveMode() {
    return !$this->config('alexanders.settings')->get('enable_sandbox');
  }

}
