<?php

namespace Drupal\alexanders\Event;

use Drupal\alexanders\Entity\AlexandersOrder;
use Symfony\Component\EventDispatcher\Event;

class ApiErrorEvent extends Event {

  const EVENT_NAME = 'alexanders_api_error_event';

  public $itemKey;

  public $message;

  public $order;

  /**
   * ApiErrorEvent constructor.
   *
   * @param \Drupal\alexanders\Entity\AlexandersOrder $order
   * @param string $itemKey
   * @param string $message
   */
  public function __construct(AlexandersOrder $order, string $itemKey, string $message) {
    $this->order = $order;
    $this->itemKey = $itemKey;
    $this->message = $message;
  }

}
