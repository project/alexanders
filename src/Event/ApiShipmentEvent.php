<?php

namespace Drupal\alexanders\Event;

use Drupal\alexanders\Entity\AlexandersOrder;
use Symfony\Component\EventDispatcher\Event;

class ApiShipmentEvent extends Event {

  const EVENT_NAME = 'alexanders_api_shipment_event';

  public $shipMethod;

  public $carrier;

  public $trackingNumber;

  public $dateShipped;

  public $cost;

  public $order;

  /**
   * ApiShipmentEvent constructor.
   *
   * @param \Drupal\alexanders\Entity\AlexandersOrder $order
   * @param string $shipMethod
   * @param string $carrier
   * @param string $trackingNumber
   * @param int $dateShipped
   * @param int $cost
   */
  public function __construct(AlexandersOrder $order, string $shipMethod, string $carrier, string $trackingNumber, int $dateShipped, int $cost) {
    $this->order = $order;
    $this->shipMethod = $shipMethod;
    $this->carrier = $carrier;
    $this->trackingNumber = $trackingNumber;
    $this->dateShipped = $dateShipped;
    $this->cost = $cost;
  }

}
