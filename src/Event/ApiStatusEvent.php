<?php

namespace Drupal\alexanders\Event;

use Drupal\alexanders\Entity\AlexandersOrder;
use Symfony\Component\EventDispatcher\Event;

class ApiStatusEvent extends Event {

  const EVENT_NAME = 'alexanders_api_status_event';

  /**
   * Due date in epoch.
   *
   * @var int
   */
  public $dueDate;

  public $order;

  /**
   * ApiStatusEvent constructor.
   *
   * @param \Drupal\alexanders\Entity\AlexandersOrder $order
   * @param int $dueDate
   */
  public function __construct(AlexandersOrder $order, int $dueDate) {
    $this->order = $order;
    $this->dueDate = $dueDate;
  }

}
