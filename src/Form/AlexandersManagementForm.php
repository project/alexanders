<?php

namespace Drupal\alexanders\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Basic settings form for Alexanders.
 *
 * Provides configuration form to toggle fields on product variation types.
 *
 * Class AlexandersManagementForm
 *
 * @package Drupal\alexanders\Form
 */
class AlexandersManagementForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alexanders_management_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['alexanders.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('alexanders.settings');
    $form['apikey'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Server API Keys'),
      '#description' => $this->t('These keys go to Alexanders so they can authenticate with your site.'),
    ];

    $form['apikey']['real'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Real API Key'),
      '#description' => $this->t('Actual API key that will cause the site to process data'),
      '#value' => $config->get('real_api_key'),
      '#attributes' => $config->get('sandbox_api_key') ? ['readonly' => 'readonly'] : [],
      '#required' => TRUE,
    ];

    $form['apikey']['sandbox'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sandbox API Key'),
      '#description' => $this->t('API key to use during testing & development'),
      '#value' => $config->get('sandbox_api_key'),
      '#attributes' => $config->get('sandbox_api_key') ? ['readonly' => 'readonly'] : [],
      '#required' => TRUE,
    ];

    $form['clientkeys'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Client configuration'),
    ];
    $form['clientkeys']['client_apikey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client API Key'),
      '#description' => $this->t('API key to connect to the Alexanders API'),
      '#default_value' => $config->get('client_apikey'),
    ];
    $form['enable_sandbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Alexanders Sandbox'),
      '#description' => $this->t('Do not use real API for testing purposes.'),
      '#default_value' => $config->get('enable_sandbox'),
    ];
    $form['verbose_log'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable verbose logging'),
      '#default_value' => (bool) $config->get('verbose_log'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('alexanders.settings')
      ->set('real_api_key', $form_state->getValue('real'))
      ->set('sandbox_api_key', $form_state->getValue('sandbox'))
      ->set('client_apikey', $form_state->getValue('client_apikey'))
      ->set('enable_sandbox', $form_state->getValue('enable_sandbox'))
      ->set('verbose_log', $form_state->getValue('verbose_log'))
      ->save();
  }

}
