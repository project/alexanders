<?php

namespace Drupal\Tests\commerce_alexanders\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\Tests\commerce_order\Functional\OrderBrowserTestBase;

/**
 * Tests admin screen.
 *
 * @group alexanders
 *
 * This is not a legacy test but installing commerce triggers lots and lots of
 * deprecations.
 * @group legacy
 */
class AdminTest extends OrderBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'commerce_alexanders',
  ];

  /**
   * Tests the admin form.
   */
  public function testAdmin() {
    $page = $this->getSession()->getPage();
    $assert = $this->assertSession();

    $this->drupalGet('admin/commerce/config/alexanders');
    $this->assertSession()->statusCodeEquals(403);

    // Create an admin user.
    $this->drupalLogin($this->createUser(['manage alexanders printing api']));
    $this->drupalGet('admin/commerce/config/alexanders');
    $this->assertSession()->statusCodeEquals(200);

    $page->selectFieldOption('order_state', 'completed');
    $page->checkField('product_types[default]');
    $page->checkField('order_item_types[default]');
    $page->pressButton('Save');

    // Test that the fields and checkboxes have been set.
    $assert->checkboxChecked('product_types[default]');
    $assert->checkboxChecked('order_item_types[default]');
    $this->assertNotNull(FieldConfig::loadByName('commerce_order_item', 'default', 'alxdr_file_url'), 'Order item field created');
    $this->assertNotNull(FieldConfig::loadByName('commerce_product', 'default', 'alxdr_send'), 'Product field created');
    $assert->fieldValueEquals('order_state', 'completed');

    // Uncheck the options and ensure the fields have been deleted.
    $page->uncheckField('product_types[default]');
    $page->uncheckField('order_item_types[default]');
    $page->pressButton('Save');

    // Reset test runner caches so latest configuration is used.
    $this->rebuildAll();

    $assert->checkboxNotChecked('product_types[default]');
    $assert->checkboxNotChecked('order_item_types[default]');
    $this->assertNull(FieldConfig::loadByName('commerce_order_item', 'default', 'alxdr_file_url'), 'Order item field deleted');
    $this->assertNull(FieldConfig::loadByName('commerce_product', 'default', 'alxdr_send'), 'Product field deleted');
  }

}
