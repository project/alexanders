<?php

namespace Drupal\alexanders_test;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class StubController {

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param $folder
   * @param $part1
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function get(Request $request, $folder, $part1, $part2, $part3) {
    $method = $request->getMethod() === 'GET' ? NULL : $request->getMethod();
    if ($part1 === FALSE) {
      $part1 = 'create';
    }
    $parts = array_filter([$part1, $part2, $part3, $method]);

    $filename = realpath(__DIR__ . '/..') . '/stubs/' . $folder . '/' . implode('_', $parts) . '.json';
    if (!file_exists($filename)) {
      $message = sprintf("Can not find stub file: %s", $filename);
      return new JsonResponse(['message' => $message], 500);
    }
    // Use include so files can contain logic using PHP.
    ob_start();
    @include $filename;
    $data = json_decode(ob_get_clean(), TRUE);
    if (!$data) {
      throw new \RuntimeException(sprintf("Invalid JSON in stub file: %s", $filename));
    }
    return new JsonResponse($data, $data['status'] ?? 200);
  }

}
