<?php

namespace Drupal\Tests\alexanders\Functional;

use Drupal\alexanders\Entity\AlexandersOrder;
use Drupal\alexanders\Entity\AlexandersOrderItem;
use Drupal\alexanders\Entity\AlexandersShipment;
use Drupal\Tests\BrowserTestBase;
use GuzzleHttp\Cookie\CookieJar;

/**
 * Functional tests for Alexanders modules site endpoints.
 *
 * @group alexanders
 */
class AlexanderApiSelfTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'alexanders',
    'system',
    'datetime',
    'address',
  ];

  /**
   * The base path to the Alexanders API endpoints.
   *
   * @var string
   */
  protected $url;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $config = $this->config('alexanders.settings');
    $config->set('real_api_key', 'alex-functionaltests');
    $config->save();
    $this->url = $this->baseUrl . '/alexanders';
    AlexandersOrder::create([
      'order_number' => 1,
      'orderItems' => [
        AlexandersOrderItem::create([
          'sku' => $this->randomString(),
          'quantity' => 1,
          'file' => 'example.com',
          'foil' => 'example.com',
        ]),
      ],
      'shipping' => [
        AlexandersShipment::create([
          'method' => 'Test',
          'address' => [],
        ]),
      ],
    ])->save();
  }

  /**
   * Tests endpoints for a 200 OKAY.
   */
  public function testAlexandersSiteEndpoints() {
    $client = NULL;
    $options = [
      'http_errors' => FALSE,
      'headers' => [
        'X-API-KEY' => 'alex-functionaltests',
        'Content-Type' => 'application/json',
      ],
      'cookies' => $this->getGuzzleCookieJar(),
    ];

    // Verify we can't GET resources.
    $response = $this->getHttpClient()->get($this->url . '/printing/1', $options);
    self::assertEquals(405, $response->getStatusCode());

    // With the correct key in live mode.
    $this->makeRequests(200, $options);

    // With the NO key in live mode.
    $options['headers']['X-API-KEY'] = '';
    $this->makeRequests(403, $options);

    // With the wrong key in live mode.
    $options['headers']['X-API-KEY'] = 'alex-wrong-key';
    $this->makeRequests(403, $options);

    // With fix key in live mode to test caching.
    $config = $this->config('alexanders.settings');
    $config->set('real_api_key', 'alex-wrong-key')->save();
    $this->makeRequests(200, $options);

    // Change to sandbox mode but use the live key.
    $config->set('enable_sandbox', TRUE)
      ->set('sandbox_api_key', 'sandbox-key')
      ->save();
    $this->makeRequests(403, $options);

    // With no key in sandbox mode.
    $options['headers']['X-API-KEY'] = '';
    $this->makeRequests(403, $options);

    // With correct key in sandbox mode.
    $options['headers']['X-API-KEY'] = 'sandbox-key';
    $this->makeRequests(200, $options);

    // With no key has the expected response code and message.
    unset($options['headers']['X-API-KEY']);
    $response = $this->getHttpClient()->put($this->url . '/printing/1', $options);
    self::assertEquals(403, $response->getStatusCode());
    $this->assertSame('Missing X-API-KEY', json_decode($response->getBody())->message);

    // With correct key in sandbox mode but malformed json.
    $options['headers']['X-API-KEY'] = 'sandbox-key';
    $response = $this->getHttpClient()->put($this->url . '/printing/1', $options);
    self::assertEquals(400, $response->getStatusCode());
    $this->assertSame('Invalid JSON', json_decode($response->getBody())->message);

    $response = $this->getHttpClient()->put($this->url . '/shipped/1', $options);
    self::assertEquals(400, $response->getStatusCode());
    $this->assertSame('Invalid JSON', json_decode($response->getBody())->message);

    $response = $this->getHttpClient()->put($this->url . '/error/1', $options);
    self::assertEquals(400, $response->getStatusCode());
    $this->assertSame('Invalid JSON', json_decode($response->getBody())->message);

    // With valid json but missing properties.
    $response = $this->getHttpClient()->put($this->url . '/printing/1', ['body' => json_encode(['foo' => 'bar'])] + $options);
    self::assertEquals(400, $response->getStatusCode());
    $this->assertSame('Invalid JSON, missing dueDate', json_decode($response->getBody())->message);

    $response = $this->getHttpClient()->put($this->url . '/shipped/1', ['body' => json_encode(['foo' => 'bar'])] + $options);
    self::assertEquals(400, $response->getStatusCode());
    $this->assertSame('Invalid JSON, missing trackingNumber, dateShipped, cost, shipMethod, carrier', json_decode($response->getBody())->message);

    $response = $this->getHttpClient()->put($this->url . '/shipped/1', ['body' => json_encode(['carrier' => 'bar'])] + $options);
    self::assertEquals(400, $response->getStatusCode());
    $this->assertSame('Invalid JSON, missing trackingNumber, dateShipped, cost, shipMethod', json_decode($response->getBody())->message);

    $response = $this->getHttpClient()->put($this->url . '/error/1', ['body' => json_encode(['foo' => 'bar'])] + $options);
    self::assertEquals(400, $response->getStatusCode());
    $this->assertSame('Invalid JSON, missing message, itemKey', json_decode($response->getBody())->message);

    // Test what happens in maintenance mode.
    \Drupal::state()->set('system.maintenance_mode', TRUE);
    $response = $this->getHttpClient()->put($this->url . '/printing/1', ['body' => json_encode(['dueDate' => '2019-01-16T19:53:37.469Z'])] + $options);
    self::assertEquals(503, $response->getStatusCode());
    $this->assertSame('Drupal is currently under maintenance. We should be back shortly. Thank you for your patience.', (string) $response->getBody());
  }

  /**
   * Make requests against the API.
   *
   * @param $expected_code
   * @param $options
   */
  protected function makeRequests($expected_code, $options) {
    // Run through PUT endpoints.
    /** @var \Psr\Http\Message\ResponseInterface $response */
    $response = $this->getHttpClient()->put($this->url . '/printing/1', ['body' => json_encode(['dueDate' => '2019-01-16T19:53:37.469Z'])] + $options);
    self::assertEquals($expected_code, $response->getStatusCode());
    // Ensure 403s return valid JSON.
    if ($expected_code === 403) {
      $this->assertSame('Invalid X-API-KEY', json_decode($response->getBody())->message);
    }

    $response = $this->getHttpClient()->put($this->url . '/shipped/1', ['body' => json_encode(['trackingNumber' => '123', 'dateShipped' => '2019-01-16T19:53:37.469Z', 'cost' => 100, 'carrier' => 'test', 'shipMethod' => 'Test'])] + $options);
    self::assertEquals($expected_code, $response->getStatusCode());
    // Ensure 403s return valid JSON.
    if ($expected_code === 403) {
      $this->assertSame('Invalid X-API-KEY', json_decode($response->getBody())->message);
    }

    $response = $this->getHttpClient()->put($this->url . '/error/1', ['body' => json_encode(['message' => 'test', 'itemKey' => '123'])] + $options);
    self::assertEquals($expected_code, $response->getStatusCode());
    // Ensure 403s return valid JSON.
    if ($expected_code === 403) {
      $this->assertSame('Invalid X-API-KEY', json_decode($response->getBody())->message);
    }
  }

  /**
   * Creates a new Guzzle CookieJar with a Xdebug cookie if necessary.
   *
   * @return \GuzzleHttp\Cookie\CookieJar
   *   The Guzzle CookieJar.
   */
  protected function getGuzzleCookieJar() {
    // @todo Add xdebug cookie.
    $cookies = $this->extractCookiesFromRequest(\Drupal::request());
    foreach ($cookies as $cookie_name => $values) {
      $cookies[$cookie_name] = $values[0];
    }
    return CookieJar::fromArray($cookies, $this->baseUrl);
  }

}
