<?php

namespace Drupal\Tests\alexanders\Functional;

use Drupal\alexanders\Entity\AlexandersOrder;
use Drupal\alexanders\Entity\AlexandersOrderItem;
use Drupal\alexanders\Entity\AlexandersShipment;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Functional tests for Alexanders modules site endpoints.
 *
 * @group alexanders
 */
class AlexandersClientTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'alexanders',
    'alexanders_test',
  ];

  /**
   * @var \Drupal\alexanders\Entity\AlexandersOrder
   */
  protected $order;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->order = AlexandersOrder::create([
      'order_number' => 1,
      'standardPrintItems' => [
        AlexandersOrderItem::create([
          'sku' => $this->randomString(),
          'quantity' => 1,
          'file' => 'example.com',
          'foil' => 'example.com',
          'width' => '6.2178',
          'height' => '4.4594',
        ]),
      ],
      'shipping' => [
        AlexandersShipment::create([
          'method' => 'Test',
          'address' => [],
        ]),
      ],
    ]);
    $this->order->save();
  }

  public function testApi() {
    /** @var \Drupal\alexanders_test\TestLogger $logger */
    $logger = \Drupal::service('logger.alexanders_test');
    $logger->clear();

    // ::createOrder fail with verbose logging.
    try {
      \Drupal::service('alexanders.api')->createOrder($this->order, TRUE);
      $this->fail('Expected exception not thrown');
    }
    catch (\Exception $e) {
      $this->assertContains('500 Internal Server Error', $e->getMessage());
    }
    $notices = $logger->getLogs('notice');
    $this->assertContains('Data sent to Alexanders', $notices[0]);
    $this->assertContains('500 response from Alexanders (POST', $notices[1]);

    $error = $logger->getLogs('error');
    $this->assertContains('500 Internal Server Error', $error[0]);

    // ::createOrder fail with no verbose logging.
    $logger->clear();
    $this->config('alexanders.settings')->set('verbose_log', FALSE)->save();
    try {
      \Drupal::service('alexanders.api')->createOrder($this->order, TRUE);
      $this->fail('Expected exception not thrown');
    }
    catch (\Exception $e) {
      $this->assertContains('500 Internal Server Error', $e->getMessage());
    }
    $this->assertEmpty($logger->getLogs('notice'));

    $error = $logger->getLogs('error');
    $this->assertContains('500 Internal Server Error', $error[0]);

    // ::createOrder success.
    $logger->clear();
    $stub_url = Url::fromRoute('alexanders_test.stub', ['folder' => 'success']);
    // Generate some default keys.
    $this->config('alexanders.settings')
      ->set('api_url', $stub_url->setAbsolute()->toString())
      ->set('api_sandbox_url', $stub_url->setAbsolute()->toString())
      ->save();
    $result = \Drupal::service('alexanders.api')->createOrder($this->order);
    $this->assertTrue($result, 'A 200 results in a successful result');
    $this->assertEmpty($logger->getLogs('notice'));
    $this->assertEmpty($logger->getLogs('error'));

    // ::createOrder success with verbose logging.
    $this->config('alexanders.settings')->set('verbose_log', TRUE)->save();
    $result = \Drupal::service('alexanders.api')->createOrder($this->order);
    $this->assertTrue($result, 'A 200 results in a successful result');
    $notices = $logger->getLogs('notice');
    $this->assertContains('Data sent to Alexanders', $notices[0]);
    $this->assertContains('6.2178', $notices[0]);
    $this->assertContains('4.4594', $notices[0]);
    $this->assertContains('200 response from Alexanders (POST', $notices[1]);
    $this->assertEmpty($logger->getLogs('error'));

    // ::deleteOrder success.
    $logger->clear();
    $result = \Drupal::service('alexanders.api')->deleteOrder($this->order);
    $this->assertTrue($result, 'A 200 results in a successful result');
    $notices = $logger->getLogs('notice');
    $this->assertContains('200 response from Alexanders (DELETE', $notices[0]);
    $this->assertEmpty($logger->getLogs('error'));

    // ::deleteOrder fail.
    $logger->clear();
    $fail_order = AlexandersOrder::create([
      'order_number' => 2,
      'shipping' => [
        AlexandersShipment::create([
          'method' => 'Test',
          'address' => [],
        ]),
      ],
    ]);
    $fail_order->save();
    $result = \Drupal::service('alexanders.api')->deleteOrder($fail_order);
    $this->assertFalse($result, 'A non-200 results in a fail result');
    $notices = $logger->getLogs('notice');
    $this->assertContains('500 response from Alexanders (DELETE', $notices[0]);
    $error = $logger->getLogs('error');
    $this->assertContains('500 Internal Server Error', $error[0]);

    // ::updateOrder success.
    $logger->clear();
    $result = \Drupal::service('alexanders.api')->updateOrder($this->order);
    $this->assertTrue($result, 'A 200 results in a successful result');
    $notices = $logger->getLogs('notice');
    $this->assertContains('Data sent to Alexanders', $notices[0]);
    $this->assertContains('200 response from Alexanders (PUT', $notices[1]);
    $this->assertEmpty($logger->getLogs('error'));

    // ::updateOrder fail.
    $logger->clear();
    $result = \Drupal::service('alexanders.api')->updateOrder($fail_order);
    $this->assertFalse($result, 'A non-200 results in a fail result');
    $notices = $logger->getLogs('notice');
    $this->assertContains('Data sent to Alexanders', $notices[0]);
    $this->assertContains('500 response from Alexanders (PUT', $notices[1]);
    $error = $logger->getLogs('error');
    $this->assertContains('500 Internal Server Error', $error[0]);
  }

}
