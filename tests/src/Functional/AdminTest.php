<?php

namespace Drupal\Tests\alexanders\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests admin screen.
 *
 * @group alexanders
 */
class AdminTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'alexanders',
  ];

  public function testAdmin() {
    $config = $this->config('alexanders.settings');
    $this->assertRegExp('/^alex-[a-zA-Z0-9\-_]{40}/', $config->get('real_api_key'));
    $this->assertRegExp('/^sandbox-[a-zA-Z0-9\-_]{40}/', $config->get('sandbox_api_key'));

    $this->drupalGet('admin/alexanders/config');
    $this->assertSession()->statusCodeEquals(403);

    // Create an admin user
    $this->drupalLogin($this->createUser(['manage alexanders printing api']));
    $this->drupalGet('admin/alexanders/config');
    $this->assertSession()->statusCodeEquals(200);

    // Pressing the save button generates configuration.
    $this->getSession()->getPage()->pressButton('Save');

  }

}
