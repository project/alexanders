<?php

namespace Drupal\Tests\alexanders\Functional;

use Drupal\alexanders\Entity\AlexandersOrder;
use Drupal\alexanders\Entity\AlexandersOrderItem;
use Drupal\alexanders\Entity\AlexandersShipment;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests entity access.
 *
 * @group alexanders
 */
class OrderAccessTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'alexanders',
  ];

  public function testAccess() {
    $order = AlexandersOrder::create([
      'order_number' => 1,
      'orderItems' => [
        AlexandersOrderItem::create([
          'sku' => $this->randomString(),
          'quantity' => 1,
          'file' => 'example.com',
          'foil' => 'example.com',
        ]),
      ],
      'shipping' => [
        AlexandersShipment::create([
          'method' => 'Test',
          'address' => [],
        ]),
      ],
    ]);
    $order->save();

    // Anonymous users have no access.
    $this->drupalGet($order->toUrl());
    $this->assertSession()->statusCodeEquals(403);

    // Users with 'view alexanders_order' can access them.
    $this->drupalLogin($this->createUser(['view alexanders_order']));
    $this->drupalGet($order->toUrl());
    $this->assertSession()->statusCodeEquals(200);

    // Logged in users can not access Alexanders orders.
    $this->drupalLogin($this->createUser());
    $this->drupalGet($order->toUrl());
    $this->assertSession()->statusCodeEquals(403);
  }

}
