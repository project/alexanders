<?php

namespace Drupal\Tests\alexanders\Functional;

use Drupal\alexanders\Entity\AlexandersOrder;
use Drupal\alexanders\Entity\AlexandersOrderItem;
use Drupal\alexanders\Entity\AlexandersShipment;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests order edit.
 *
 * @group alexanders
 */
class OrderEditTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'alexanders',
  ];

  public function testAccess() {
    $order = AlexandersOrder::create([
      'order_number' => 1,
      'orderItems' => [
        AlexandersOrderItem::create([
          'sku' => $this->randomString(),
          'quantity' => 1,
          'file' => 'example.com',
          'foil' => 'example.com',
        ]),
      ],
      'shipping' => [
        AlexandersShipment::create([
          'method' => 'Test',
          'address' => [],
        ]),
      ],
    ]);
    $order->save();

    $edit_url = $order->toUrl('edit-form');
    $this->drupalGet($edit_url);
    $this->assertSession()->statusCodeEquals(403);

    // Users with 'manage alexanders printing api' can edit them.
    $this->drupalLogin($this->createUser(['manage alexanders printing api']));
    $this->drupalGet($edit_url);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Due Date: Waiting for Alexanders');

    $order->setDue(1562249454);
    $order->save();
    $this->drupalGet($edit_url);
    // Testing infrastructure sets the timezone so this assert is okay.
    $this->assertSession()->pageTextContains('Due Date: Fri, 07/05/2019');
  }

}
